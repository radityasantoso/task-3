#include <stdio.h>
#include <math.h>


void main(){
    int x;
    float y, z;
    
    printf("Formula P(x) = x^4 + 7x^3 - 5x + 9   \n\n");
    printf("Masukkan nilai x = ");
    scanf("%d", &x);
    
    y=(pow(x,4))+(7*pow(x,3)-(5*x))+9;
    z=((pow(y,2)) + (6*pow(y,2)) - (4*pow(x,2)) / (9*y));
    
    printf("Didapatkan nilai y = %2.f dan nilai z = %f\n", y, z);
}
